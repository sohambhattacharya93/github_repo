package bhattacharya.soham.githubrepo.Model;

/**
 * Created by sohambhattacharya on 29/10/17.
 */


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Repository {

    @SerializedName("items")
    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
