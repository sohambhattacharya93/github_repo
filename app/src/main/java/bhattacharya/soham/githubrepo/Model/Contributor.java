package bhattacharya.soham.githubrepo.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sohambhattacharya on 29/10/17.
 */

public class Contributor {

    String login;
    String avatar_url;
    String repos_url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getRepos_url() {
        return repos_url;
    }

    public void setRepos_url(String repos_url) {
        this.repos_url = repos_url;
    }


    public Contributor(String login, String avatar_url, String repos_url) {
        this.login = login;
        this.avatar_url = avatar_url;
        this.repos_url = repos_url;
    }

    public Contributor() {
    }
}
