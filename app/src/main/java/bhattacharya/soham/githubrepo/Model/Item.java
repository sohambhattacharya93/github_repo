package bhattacharya.soham.githubrepo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sohambhattacharya on 29/10/17.
 */

public class Item {


    public Item() {
    }

    @SerializedName("name")
    private String name;
    @SerializedName("full_name")
    private String full_name;
    @SerializedName("watchers_count")
    private int watchers_count;
    @SerializedName("description")
    private String description;
    @SerializedName("html_url")
    private String html_url;
    @SerializedName("contributors_url")
    private String contributors_url;
    @SerializedName("owner")
    private Owner owner;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getContributors_url() {
        return contributors_url;
    }

    public void setContributors_url(String contributors_url) {
        this.contributors_url = contributors_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getWatchers_count() {
        return watchers_count;
    }

    public void setWatchers_count(int watchers_count) {
        this.watchers_count = watchers_count;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Item(String name, String full_name, int watchers_count, String description, String html_url, String contributors_url, Owner owner) {
        this.name = name;
        this.full_name = full_name;
        this.watchers_count = watchers_count;
        this.description = description;
        this.html_url = html_url;
        this.contributors_url = contributors_url;
        this.owner = owner;
    }

    public class Owner
    {
        @SerializedName("avatar_url")
        private String avatar_url;

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }
    }
}
