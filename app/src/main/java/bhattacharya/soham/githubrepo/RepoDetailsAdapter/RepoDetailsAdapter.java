package bhattacharya.soham.githubrepo.RepoDetailsAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import bhattacharya.soham.githubrepo.ContributorDetails;
import bhattacharya.soham.githubrepo.Model.Contributor;
import bhattacharya.soham.githubrepo.Model.Item;
import bhattacharya.soham.githubrepo.R;
import bhattacharya.soham.githubrepo.RepoDetails;

/**
 * Created by sohambhattacharya on 30/10/17.
 */

public class RepoDetailsAdapter extends RecyclerView.Adapter<bhattacharya.soham.githubrepo.RepoDetailsAdapter.RepoDetailsAdapter.RepoHolder>{

    private Context mContext;
    private List<Contributor> ContributorList;

    public class RepoHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView avatar_image;

        public RepoHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            avatar_image = (ImageView) view.findViewById(R.id.avatar_image);
        }
    }


    public RepoDetailsAdapter(Context mContext, List<Contributor> Clist) {
        this.mContext = mContext;
        this.ContributorList = Clist;
    }


    @Override
    public bhattacharya.soham.githubrepo.RepoDetailsAdapter.RepoDetailsAdapter.RepoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contributor_card, parent, false);

        return new bhattacharya.soham.githubrepo.RepoDetailsAdapter.RepoDetailsAdapter.RepoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final bhattacharya.soham.githubrepo.RepoDetailsAdapter.RepoDetailsAdapter.RepoHolder holder, int position) {
        final Contributor contributor = ContributorList.get(position);
        holder.name.setText(contributor.getLogin());


        // loading album cover using Glide library
        Glide.with(mContext).load(contributor.getAvatar_url()).into(holder.avatar_image);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, ContributorDetails.class);
                intent.putExtra("name", contributor.getLogin());
                intent.putExtra("avatar_image", contributor.getAvatar_url());
                intent.putExtra("repo_url", contributor.getRepos_url());
                mContext.startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

    }


    @Override
    public int getItemCount() {
        return ContributorList.size();
    }


}

