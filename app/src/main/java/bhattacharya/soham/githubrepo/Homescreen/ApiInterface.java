package bhattacharya.soham.githubrepo.Homescreen;


import bhattacharya.soham.githubrepo.Model.Repository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by sohambhattacharya on 29/10/17.
 */

public interface ApiInterface {
    @GET("repositories")
    Call<Repository> getSearchResults(@Query("q") String query, @Query("per_page") String per_page,@Query("sort") String sort,@Query("order") String order);


}
