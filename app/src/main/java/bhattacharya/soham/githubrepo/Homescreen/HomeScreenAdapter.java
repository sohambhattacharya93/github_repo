package bhattacharya.soham.githubrepo.Homescreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import bhattacharya.soham.githubrepo.Model.Item;
import bhattacharya.soham.githubrepo.Model.Repository;
import bhattacharya.soham.githubrepo.R;
import bhattacharya.soham.githubrepo.RepoDetails;

/**
 * Created by sohambhattacharya on 28/10/17.
 */

public class HomeScreenAdapter extends RecyclerView.Adapter<HomeScreenAdapter.RepoHolder>{

    private Context mContext;
    private List<Item> RepoList;

    public class RepoHolder extends RecyclerView.ViewHolder {
        public TextView name, full_name,watchers_count,commit_count;
        public ImageView avatar_image;

        public RepoHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            full_name = (TextView) view.findViewById(R.id.full_name);
            watchers_count = (TextView) view.findViewById(R.id.watcher_count);
            avatar_image = (ImageView) view.findViewById(R.id.avatar_image);
        }
    }


    public HomeScreenAdapter(Context mContext, List<Item> Rlist) {
        this.mContext = mContext;
        this.RepoList = Rlist;
    }


    @Override
    public RepoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_card, parent, false);

        return new RepoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RepoHolder holder, int position) {
       final Item item = RepoList.get(position);
        holder.name.setText(item.getName());
        holder.full_name.setText(item.getFull_name());
        holder.watchers_count.setText("Watcher's Count :  "+String.valueOf(item.getWatchers_count()));
       // holder.commit_count.setText(item.get());

        // loading album cover using Glide library
        Glide.with(mContext).load(item.getOwner().getAvatar_url()).into(holder.avatar_image);


                holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, RepoDetails.class);
                intent.putExtra("name", item.getName());
                intent.putExtra("full_name", item.getFull_name());
                intent.putExtra("avatar_image", item.getOwner().getAvatar_url());
                intent.putExtra("watchers_count", String.valueOf(item.getWatchers_count()));
                intent.putExtra("description", item.getDescription());
                intent.putExtra("html_url", item.getHtml_url());
                intent.putExtra("contributors_url", item.getContributors_url());
                mContext.startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });
    }


    @Override
    public int getItemCount() {
        return RepoList.size();
    }


}
