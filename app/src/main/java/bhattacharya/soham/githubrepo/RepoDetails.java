package bhattacharya.soham.githubrepo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import bhattacharya.soham.githubrepo.Homescreen.HomeScreenAdapter;
import bhattacharya.soham.githubrepo.Model.Contributor;
import bhattacharya.soham.githubrepo.Model.Item;
import bhattacharya.soham.githubrepo.RepoDetailsAdapter.RepoDetailsAdapter;
import bhattacharya.soham.githubrepo.Utility.CircularProgressView;
import bhattacharya.soham.githubrepo.Utility.ConnectionDetector;
import bhattacharya.soham.githubrepo.Utility.Utilities;

/**
 * Created by sohambhattacharya on 29/10/17.
 */

public class RepoDetails  extends AppCompatActivity {

    String name, full_name, description, project_link, watchers_count, avatar_url,contributors_url;

    TextView name_view, full_name_view, description_view,
            project_link_view, watchers_count_view;
    ImageView avatar_image;
    private List<Contributor> ContributorsList;
    private RecyclerView recyclerView;
    private RepoDetailsAdapter adapter;
    CircularProgressView progressView;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repodetails_layout);

        name_view=(TextView)findViewById(R.id.name);
        full_name_view=(TextView)findViewById(R.id.full_name);
        description_view=(TextView)findViewById(R.id.description);
        project_link_view=(TextView)findViewById(R.id.project_link);
        watchers_count_view=(TextView)findViewById(R.id.watchers_count);
        avatar_image=(ImageView)findViewById(R.id.avatar_image);
        progressView = (CircularProgressView) findViewById(R.id.progressView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
             name = extras.getString("name");
            full_name = extras.getString("full_name");
            description = extras.getString("description");
            project_link = extras.getString("html_url");
            watchers_count = extras.getString("watchers_count");
            avatar_url = extras.getString("avatar_image");
            contributors_url = extras.getString("contributors_url");
        }

        name_view.setText(name);
        full_name_view.setText(full_name);
        description_view.setText(description);
        project_link_view.setText(project_link);
        watchers_count_view.setText(watchers_count);
        project_link_view.setClickable(true);

        project_link_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RepoDetails.this, ProjectLink.class);
                intent.putExtra("url", project_link);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        Glide.with(RepoDetails.this).load(avatar_url).into(avatar_image);

        Utilities.startAnimationThreadStuff(500, progressView);
        progressView.setIndeterminate(true);
        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectingToInternet();
        ContributorsList = new ArrayList<Contributor>();
        if (isInternetPresent) {
            new GetContributors().execute();
        } else {
            showAlertDialog(getApplicationContext(), getResources().getString(R.string.no_internet_title_text),
                    getResources().getString(R.string.no_internet_message_text), false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_out_left,R.anim.slide_in_left);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_left,R.anim.slide_in_left);
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Showing Alert Message
        alertDialog.show();
        progressView.setVisibility(View.GONE);
    }



    private class GetContributors extends AsyncTask<String, String, String> {

        String  url=contributors_url, result = "",s="";
        Boolean proceed = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {

            try {
                URL url = new URL(contributors_url);
                HttpURLConnection conexion = (HttpURLConnection) url
                        .openConnection();
                conexion.setRequestProperty("User-Agent",
                        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
                if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(conexion.getInputStream()));
                    String linea = reader.readLine();
                    while (linea != null) {
                        s += linea;
                        linea = reader.readLine();
                    }
                    reader.close();

                    result = s;
                } else {
                    conexion.disconnect();
                    return null;
                }
                conexion.disconnect();
                proceed=true;
                return result;

            } catch (Exception e)
            {

            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            Log.i("result", result);
            if (proceed) {


                ContributorsList = new ArrayList<>();
                try {
                    JSONArray arr = new JSONArray(result);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj = arr.getJSONObject(i);
                        Contributor c = new Contributor();
                        c.setAvatar_url(obj.getString("avatar_url"));
                        c.setRepos_url(obj.getString("repos_url"));
                        c.setLogin(obj.getString("login"));
                        ContributorsList.add(c);
                    }
                        adapter = new RepoDetailsAdapter(RepoDetails.this, ContributorsList);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(RepoDetails.this, 3);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.addItemDecoration(new RepoDetails.SimpleDividerItemDecoration(RepoDetails.this));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(adapter);
                        progressView.setVisibility(View.GONE);

                }
                catch(Exception e)
                    {

                    }
                }
            }

        }
    public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public SimpleDividerItemDecoration(Context context) {
            mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }
    }


