package bhattacharya.soham.githubrepo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bhattacharya.soham.githubrepo.Homescreen.ApiClient;
import bhattacharya.soham.githubrepo.Homescreen.ApiInterface;
import bhattacharya.soham.githubrepo.Homescreen.HomeScreenAdapter;
import bhattacharya.soham.githubrepo.Model.*;
import bhattacharya.soham.githubrepo.Utility.CircularProgressView;
import bhattacharya.soham.githubrepo.Utility.ConnectionDetector;
import bhattacharya.soham.githubrepo.Utility.Utilities;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private HomeScreenAdapter adapter;
    EditText editText;
    private List<Item> RepoList;
    private static final String TAG = MainActivity.class.getSimpleName();
    ApiInterface apiService;
    CircularProgressView progressView;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreen_layout);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        editText = findViewById(R.id.editTextSearch);
        progressView = (CircularProgressView) toolbar.findViewById(R.id.progressView);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService =
                ApiClient.getClient().create(ApiInterface.class);

        try {
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    cd = new ConnectionDetector(MainActivity.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {


                        if (editText.getText().length() > 2) {
                            progressView.setVisibility(View.VISIBLE);
                            Utilities.startAnimationThreadStuff(500, progressView);
                            progressView.setIndeterminate(true);

                            Call<Repository> call = apiService.getSearchResults(editText.getText().toString(), "10", "stars", "desc");

                            call.enqueue(new Callback<Repository>() {
                                @Override
                                public void onResponse(Call<Repository> call, Response<Repository> response) {
                                    try {
                                        if (response != null) {
                                            List<Item> repos = response.body().getItems();
                                            RepoList = new ArrayList<>();
                                            for (int i = 0; i < repos.size(); i++) {
                                                Item item = new Item();
                                                item.setFull_name(repos.get(i).getFull_name());
                                                item.setName(repos.get(i).getName());
                                                item.setWatchers_count(repos.get(i).getWatchers_count());
                                                item.setOwner(repos.get(i).getOwner());
                                                item.setContributors_url(repos.get(i).getContributors_url());
                                                item.setDescription(repos.get(i).getDescription());
                                                item.setHtml_url(repos.get(i).getHtml_url());
                                                RepoList.add(item);
                                                adapter = new HomeScreenAdapter(MainActivity.this, RepoList);
                                                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MainActivity.this, 1);
                                                recyclerView.setLayoutManager(mLayoutManager);
                                                recyclerView.addItemDecoration(new SimpleDividerItemDecoration(MainActivity.this));
                                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                                recyclerView.setAdapter(adapter);
                                                progressView.setVisibility(View.GONE);
                                            }
                                        }
                                    } catch (Exception e) {
                                        Toast.makeText(MainActivity.this, "API rate limit exceeded", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<bhattacharya.soham.githubrepo.Model.Repository> call, Throwable t) {
                                    Toast.makeText(MainActivity.this, "Exception Occurred", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } else {
                        showAlertDialog(getApplicationContext(), getResources().getString(R.string.no_internet_title_text),
                                getResources().getString(R.string.no_internet_message_text), false);
                    }
                }
            });


        } catch (Exception e) {

        }

/*
        try {
            Glide.with(this).load(R.drawable.ic_launcher_background).into((ImageView) findViewById(R.id.avatar_image));
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Showing Alert Message
        alertDialog.show();
        progressView.setVisibility(View.GONE);
    }

    public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public SimpleDividerItemDecoration(Context context) {
            mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (editText.getText().length() > 0) {
            editText.setText("");
        } else {
            super.onBackPressed();
        }
    }
}
