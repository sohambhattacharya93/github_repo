package bhattacharya.soham.githubrepo.ContributorDetailsAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import bhattacharya.soham.githubrepo.ContributorDetails;
import bhattacharya.soham.githubrepo.Model.Contributor;
import bhattacharya.soham.githubrepo.Model.Repo_Model;
import bhattacharya.soham.githubrepo.R;
import bhattacharya.soham.githubrepo.RepoDetails;

/**
 * Created by sohambhattacharya on 30/10/17.
 */



public class ContributorDetailsAdapter extends RecyclerView.Adapter<bhattacharya.soham.githubrepo.ContributorDetailsAdapter.ContributorDetailsAdapter.RepoHolder>{

    private Context mContext;
    private List<Repo_Model> Repo_list;

    public class RepoHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView full_name;
        public TextView watchers_count;
        public ImageView avatar_image;

        public RepoHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            full_name = (TextView) view.findViewById(R.id.full_name);
            watchers_count = (TextView) view.findViewById(R.id.watcher_count);
            avatar_image = (ImageView) view.findViewById(R.id.avatar_image);
        }
    }


    public ContributorDetailsAdapter(Context mContext, List<Repo_Model> Rlist) {
        this.mContext = mContext;
        this.Repo_list = Rlist;
    }



    @Override
    public bhattacharya.soham.githubrepo.ContributorDetailsAdapter.ContributorDetailsAdapter.RepoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_card, parent, false);

        return new bhattacharya.soham.githubrepo.ContributorDetailsAdapter.ContributorDetailsAdapter.RepoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final bhattacharya.soham.githubrepo.ContributorDetailsAdapter.ContributorDetailsAdapter.RepoHolder holder, int position) {
        final Repo_Model repo_model = Repo_list.get(position);
        holder.name.setText(repo_model.getName());
        holder.full_name.setText(repo_model.getFull_name());
        holder.watchers_count.setText("Watchers Count : "+String.valueOf(repo_model.getWatchers_count()));
        holder.avatar_image.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, RepoDetails.class);
                intent.putExtra("name", repo_model.getName());
                intent.putExtra("full_name", repo_model.getFull_name());
                intent.putExtra("avatar_image", repo_model.getAvatar_url());
                intent.putExtra("watchers_count", String.valueOf(repo_model.getWatchers_count()));
                intent.putExtra("description", repo_model.getDescription());
                intent.putExtra("html_url", repo_model.getHtml_url());
                intent.putExtra("contributors_url", repo_model.getContributors_url());
                mContext.startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

    }


    @Override
    public int getItemCount() {
        return Repo_list.size();
    }


}